import re
import urllib
import urllib.request
import validators

setOfLinks = set()
setOfLinksAndStatus = set()
startURL = "http://www.epam.com"
fileName = 'text.txt'
urlWithHTTP = 'http'
regex = '.*?href="(.*?)"'


class MiniHTMLParser:
    def getStatusOfHtmlFile(self, url):
        status = ""
        if not validators.url(url):
            status = "invalid"
        else:
            status = "valid"
        return status

    def writeFile(self, html):
        file = open(fileName, 'a')
        file.write(str(html))
        file.close()

    def readHTML(self, url):
        page = urllib.request.urlopen(url)
        html = page.read()
        page.close()
        links = re.findall(regex, str(html))
        for link in links:
            if link.find(urlWithHTTP):
                setOfLinks.add(url + link)
            else:
                print('external link')
        print("\n".join(setOfLinks))
        return setOfLinks

    def linksCrawler(self, url):
        MiniHTMLParser.readHTML(self, url)
        for link in setOfLinks:
            status = MiniHTMLParser.getStatusOfHtmlFile(self, url)
            setOfLinksAndStatus.add(status + " " + url)


def main():
    parser = MiniHTMLParser()
    MiniHTMLParser.linksCrawler(parser, startURL)
    for link in set(setOfLinks):
        MiniHTMLParser.linksCrawler(parser, link)
    MiniHTMLParser.writeFile(parser,setOfLinksAndStatus)


if __name__ == "__main__":
    main()
